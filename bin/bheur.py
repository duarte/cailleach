#!/usr/bin/env python3
"""Some magic will be used to collect and prepare 
the configuration files to process some test beam raw 
data using Corryvreckan
"""
__author__ = "Jordi Duarte-Campderros"
__credits__ = ["Jordi Duarte-Campderros"]
__version__ = "0.1"
__maintainer__ = "Jordi Duarte-Campderros"
__email__ = "jorge.duarte.campderros@cern.ch"
__status__ = "Development"

import os
import click
import glob
import pickle

class bcolors:
    """Extracted from https://stackoverflow.com/questions/287871/how-to-print-colored-text-to-the-terminal
    therefore under under CC BY-SA 3.0
    """
    HEADER = '\033[95m'
    INFO   = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def get_templates():
    """Extract the templates path. 
    Assumption, the templates path is either in `templates` directory
    place on top of the `bin` directory where this script lives

    # XXX - THIS MUST BE CROSS-CHECKED for the installation case - XXX
    """
    templates_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)),'../templates')

    return sorted(glob.glob('{}/*.conf'.format(templates_dir)))
        
    
def actual_configuration(config_template,args):
    """
    """
    with open(config_template) as f:
        lines = f.readlines()

    # Substitute the placeholders 
    newlines=[] 
    for line in lines:
        newlines.append( line.replace('@RunNumber@', args.run_number).\
                replace('@BeamEnergy@', str(args.beam_energy)).\
                replace('@GeoFile@', args.geo_file).\
                replace('@GeoID@', args.geo_id).\
                replace('@DataPath@', args.data_path) )

    new_cfg_name = os.path.basename(config_template).replace('.conf','_{}.conf'.format(args.run_number))
    with open(new_cfg_name,'w') as fn:
        fn.writelines(newlines)

    return new_cfg_name

def readapt_to_multiple_runs(config_template,runlist):
    """
    """

def reminder(run_number):
    """
    """
    memorize_fname = get_informative_hidden_filename(run_number)
    with open(memorize_fname,'rb') as f:
        info = pickle.load(f)
    cfgs = info['cfgs']
    geo_file = info['geo_file']

    COMMAND = { 
            100: ['-o MaskCreator.mask_dead_pixels=false',
                '-o number_of_events=-1'], 
            200: [''],
            300: [ '-o AlignmentTrackChi2.align_orientation=false',                     
                '-o detectors_file="geometries/{}_align-telescope.geo" '\
                        '-o Tracking4D.spatial_cut_abs=200um,200um -o AlignmentTrackChi2.max_track_chi2ndof=20'.format(geo_file),
                '-o detectors_file="geometries/{}_align-telescope.geo" -o Tracking4D.spatial_cut_abs=100um,100um '\
                        '-o AlignmentTrackChi2.max_track_chi2ndof=10 -o Tracking4D.min_hits_on_track=5'.format(geo_file)],
            310: [ '', ''],
            320: [''], 
            350: [''],
            400: ['-o AlignmentDUTResidual.align_orientation=false', 
                '-o DUTAssociation.spatial_cut_rel=5 -o AlignmentDUTResidual.align_orientation=false',
                '-o DUTAssociation.spatial_cut_rel=5',
                '-o DUTAssociation.spatial_cut_rel=3',
                '-o DUTAssociation.spatial_cut_rel=2'
                ],
            500: [ '' ] 
            }

    ADVISE = { 
            100: ('When run with all events, de-activate the MaskCreator module for the REF and TEL',
                'Remember to activate the `mask_file` parameter in each detector of the geo file pointing to `output_GEOID/MaskCreator`',),
            200: ('Look at the output file for the correlation plots',
                ' +-- ROTZ -> 90',
                '     ColRow with positive slope AND RowCol with negative slope:  rotz -> 90',
                ' +-- ROTX -> 180 or ROTY -> 180 ',
                ),
            310: ('After all the iterations, check the telescope is aligned: run `check-alignment-telescope.conf`'\
                    ' and afterwards lauch the `summary_alignment.py` over the output file',),
            400: ('After all the iterations, check the DUT is aligned: run `check-alignment-all.conf`'\
                    ' and afterwards lauch the `summary_alignment.py` over the output file',)
            }

    print(f"{bcolors.OKCYAN}[INFO]{bcolors.ENDC}"+" Advisable sequence of commands for RUN {}".format(run_number))
    print(f"{bcolors.OKCYAN}[INFO] =================================================={bcolors.ENDC}")
    for order, cfg in cfgs.items():
        for options in COMMAND[order]:
            print("corry -c {} {}".format(cfg,options))
        if order in ADVISE:
            for comment in ADVISE[order]:
                print(f"{bcolors.HEADER}[NOTE] {bcolors.ENDC}"+"{}".format(comment))
    print(f"{bcolors.OKCYAN}[INFO] =================================================={bcolors.ENDC}")


def get_informative_hidden_filename(run_number):
    return '.bheur_run{}.info'.format(run_number)


@click.group()
def cli():
    """Some magic will be used to collect and prepare
    the configuration files to process raw test beam
    data using Corryvreckan
    """
    pass

@cli.command(name='prepare')
@click.argument('run_number')
@click.option('-b','--beam-energy', default=120, type=float, help='The energy of the beam in GeV [120]')
@click.option('-g','--geo-file',    default='geo', help='The name of the setup geometry file (without suffix, assumed .geo)')
@click.option('-i','--geo-id',     default='00', help='The corresponding GeoID')
@click.option('-d','--data-path',       
        default='/eos/cms/store/group/dpg_tracker_upgrade/BeamTestTelescope/20210901-CERN_SPS-H6A/', 
        help='The path to the raw data')
@click.option('-r','--reconfigure', is_flag=True, help='Force the re-configuration even'\
        ' if all the configs files already exist')
#@click.option('-r','--runlist', help='Extract all the needed info from a CSV file [WIP: XXX]')
def prepare_command(run_number,beam_energy,geo_file,geo_id,data_path,reconfigure):#runlist):
    """Collect and prepare all the configurations files to process the test beam data
    of the `run_number`
    """
    class dummy():
        def __init__(self,**kwargs):
            for key,value in kwargs.items():
                setattr(self,key,value)

    args = dummy(run_number=run_number,
            beam_energy=beam_energy,
            geo_file=geo_file,
            geo_id=geo_id,
            data_path=data_path)
    # A hidden file with this info
    memorize_fname = get_informative_hidden_filename(args.run_number)
    if os.path.isfile(memorize_fname):
        if reconfigure:
            print('[INFO] Force re-configuration for run {}'.format(args.run_number))
        else:
            reminder(args.run_number)

    # For all templates
    info = { 'cfgs': {}, 'geo_file': geo_file }
    for template_cfg in get_templates():
        # file is formatted as XXX_name-step.conf
        try:
            order = int(os.path.basename(template_cfg).split('_')[0])
        except ValueError:
            # just the checkers alignment configs
            continue
        info['cfgs'][order] = actual_configuration(template_cfg,args)
        
    # store this info in a hidden file
    with open(memorize_fname,'wb') as fm:
        pickle.dump(info,fm)
    reminder(args.run_number)
    

@cli.command(name="remind")
@click.argument('run_number')
def remind_command(run_number):
    """Remind what are the sequence of commands to be launch and how
    """
    # A hidden file with this info
    memorize_fname = get_informative_hidden_filename(run_number)
    if not os.path.isfile(memorize_fname):
        print("[WARNING] No configuration was performed for run {}. Lauch first the command `prepare`".format(run_number))
        return
    reminder(run_number)


if __name__ == '__main__':
    cli()

                                                                                      
