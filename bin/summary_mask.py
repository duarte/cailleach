#!/usr/bin/env python3
"""Plot the hitmap before and after the mask application
"""
__author__ = "Jordi Duarte-Campderros"
__credits__ = ["Jordi Duarte-Campderros"]
__version__ = "0.1"
__maintainer__ = "Jordi Duarte-Campderros"
__email__ = "jorge.duarte.campderros@cern.ch"
__status__ = "Development"

import sys
import click
import ROOT
from sifca_utils.utils import Bunch
import sifca_utils

@click.command()
@click.argument('input_file')
@click.option('-d','--duts-only',is_flag=True,help='Show only DUTs-like planes only')
@click.option('-n','--name-planes', default='MIMOSA26', help='The name of the planes to plot residuals [default: MIMOSA26_] ')
def main(input_file,duts_only,name_planes):
    """Plot the hitmap before and after. The MaskCreator file used as input
    """
    d = Bunch(input_file)
    mask_maps = {}
    plane = name_planes+"_"
    if filter(lambda x: x.find(plane) == 0, dir(d.MaskCreator)) == 0:
        print(f'No plane called `{plane}`.')
        print(f'Available plane names: {dir(d.MaskCreator)}')
        return 

    # Extract the mask and build the negative
    for plstr in filter(lambda x: x.find(plane) == 0, dir(d.MaskCreator)):
        plid = int(plstr.replace(plane,''))
        if duts_only and plid in range(1,7):
            continue
        pl = getattr(d.MaskCreator,plstr)
        try:
            maskmap = getattr(pl,'maskmap').Clone(f'maskmap_{plid}')
        except AttributeError:
            # Not interested
            print(f'Not available Mask for {plstr}')
            continue
        # Get the hitmap before any mask
        try: 
            mask_maps[plid] = (maskmap, getattr(getattr(d.EventLoaderEUDAQ2,plstr),'hitmap').Clone(f'hitmap_{plid}'))
        except AttributeError:
            # Not interested
            print(f'Not available Hitmap for {plstr}')
            continue
    
    hitmap_after = {}
    for axe in ["X", "Y"]:
        # Create the canvas 
        ROOT.gROOT.SetBatch(0)
        cv = ROOT.TCanvas(f'mapcomp_{axe}',f'Mask Maps {axe} planes',800,1100)
        cv.Divide(2,len(mask_maps))
        for i,(pl_id,the_map) in enumerate(mask_maps.items()):
            # The
            cv.cd(2*i+1)
            # Before 
            getattr(the_map[1],f'Projection{axe}')().Draw()
            cv.cd(2*i+2)
            # Build the map after if not preesent
            if pl_id not in hitmap_after:
                hitmap_after[pl_id] = the_map[1].Clone(f'{the_map[1].GetName()}_masked')
                _ = hitmap_after[pl_id].Reset()
                for _i in range(1, the_map[1].GetNbinsX()+1):
                    for _j in range(1, the_map[1].GetNbinsY()+1):
                        _ = hitmap_after[pl_id].SetBinContent( hitmap_after[pl_id].GetBin(_i,_j), 1 )
                # The negative mask
                hitmap_after[pl_id].Add(the_map[0],-1)
                # Add the final hitmap masked
                hitmap_after[pl_id].Multiply(the_map[1])
            getattr(hitmap_after[pl_id],f'Projection{axe}')().Draw()
        cv.Update()
        if sys.version_info.major < 3:
            print('WARNING: You should use python3...')
            raw_input('Press any key to exit ...')
        else:
            input('Press any key to exit ...')

        cv.Close()


if __name__ == '__main__':
    main()


