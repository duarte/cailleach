#!/usr/bin/env python3
"""Plot the residuals of all available planes, obtaining this
information from the Tracking4D module

The invariant assumption is that the available planes are inside
the root directory `Tracking4D`, with an integer in the name, such
as NAME_<index>
"""
__author__ = "Jordi Duarte-Campderros"
__credits__ = ["Jordi Duarte-Campderros"]
__version__ = "0.1"
__maintainer__ = "Jordi Duarte-Campderros"
__email__ = "jorge.duarte.campderros@cern.ch"
__status__ = "Development"

import sys
import click
import ROOT
from sifca_utils.utils import Bunch
import sifca_utils

@click.command()
@click.argument('input_file')
@click.option('-g','--gauss-fit',is_flag=True,help='Perform a gausian fit in the residuals')
@click.option('-d','--duts-only',is_flag=True,help='Show only DUTs-like planes only')
@click.option('-n','--name-planes', default='MIMOSA26', help='The name of the planes to plot residuals [default: MIMOSA26_] ')
def main(input_file,gauss_fit,duts_only,name_planes):
    """Plot the residuals of all available planes, obtaining this
    information from the Tracking4D module
    """
    if gauss_fit:
        ROOT.gStyle.SetOptFit(111)
        ROOT.gROOT.SetBatch()

    d = Bunch(input_file)
    residuals_map = {}
    plane = name_planes+"_"
    if filter(lambda x: x.find(plane) == 0, dir(d.Tracking4D)) == 0:
        print(f'No plane called `{plane}`.')
        print(f'Available plane names: {dir(d.Tracking4D)}')
        return 

    # Generate all residuals-like plots per available plane
    for plstr in filter(lambda x: x.find(plane) == 0, dir(d.Tracking4D)):
        plid = int(plstr.replace(plane,''))
        if duts_only and plid in range(1,7):
            continue
        pl = getattr(d.Tracking4D,plstr)
        try:
            gres = getattr(pl,'global_residuals')
        except AttributeError:
            # Not interested
            print('Not available residual information')
            continue
        residuals_map[plid] = (gres.GlobalResidualsX,gres.GlobalResidualsY)
        if gauss_fit:
            for res in residuals_map[plid]:
                _ = res.Fit('gaus','Q')
    # Create the canvas with all them
    ROOT.gROOT.SetBatch(0)
    cv = ROOT.TCanvas('cvres','Residuals X-Y planes',800,1100)
    cv.Divide(2,len(residuals_map))
    for i,(pl_id,htup) in enumerate(residuals_map.items()):
        # The x
        cv.cd(2*i+1)
        htup[0].Draw()
        cv.cd(2*i+2)
        htup[1].Draw()
    cv.Update()

    # The chi2ndof and track slopes
    cq = ROOT.TCanvas('cquality','Track quality',800,550)
    cq.Divide(2,2)
    cq.cd(1)
    d.Tracking4D.tracksPerEvent.GetXaxis().SetRangeUser(0,40)
    d.Tracking4D.tracksPerEvent.Draw()
    cq.cd(2)
    d.Tracking4D.trackChi2ndof.GetXaxis().SetRangeUser(0,20)
    d.Tracking4D.trackChi2ndof.Draw()
    cq.cd(3)
    d.Tracking4D.trackAngleX.GetXaxis().SetRangeUser(-3e-3,3e-3)
    d.Tracking4D.trackAngleX.Draw()
    cq.cd(4)
    d.Tracking4D.trackAngleY.GetXaxis().SetRangeUser(-3e-3,3e-3)
    d.Tracking4D.trackAngleY.Draw()
    cq.Update()

    if sys.version_info.major < 3:
        print('WARNING: You should use python3...')
        raw_input('Press any key to exit ...')
    else:
        input('Press any key to exit ...')
    cv.Close()


if __name__ == '__main__':
    main()


