#!/usr/bin/env python3
"""This module extracts the S-curves for the pixels to obtain the 
calibration constants from an input .dat file created by the Ph2_ACF readout
system. So far, the input file should be the one obtained from a GAIN calibration

The module contains function to plot calibration related quantities.
"""
__author__ = "Jordi Duarte-Campderros"
__credits__ = ["Jordi Duarte-Campderros"]
__version__ = "0.1"
__maintainer__ = "Jordi Duarte-Campderros"
__email__ = "jorge.duarte.campderros@cern.ch"
__status__ = "Development"

import os
from datetime import datetime
import click

import numpy as np

from progress.bar import Bar

import ROOT

import sifca_utils.plotting as pl

# FIXME: To be centralized in a file/module
# Units and ctes
ADC  = 1
e    = 1.0
kE   = 1e3*e
V    = 1.0
mV   = 1e-3*V
um   = 1.0

Coulomb2E  = 6.24150975e18*e 
Coulomb2kE = Coulomb2E/kE # kilo-electrons 
ePerMicron = 76.0*e/um

# Number of bits available for ToT 
nTOT = 2**4

# The default Vref value (in V)
VREF = '0.9'

# Describe where is the last TOT available bit (saturation bit)
TOT_BIT_SATURATION = 14


# Calibration curve for VCal, assuming a capacitance of the
# injection circuit of 8.21+-0.04 fF: https://twiki.cern.ch/twiki/bin/view/RD53/BiasesNDacs53A
# --- For Vref=0.8 V
class QDAC_Conversor(object):
    # A:= conversion factor from DVcal to electrons. [A]=Electrons/DVcal
    _avref08 = 10.1*e/ADC
    # B:= Offset, (noise) [B]=Electrons
    _bvref08 = 50.0*e
    # --- For Vref=0.9 V (see 20200916_CNM3D_DESY2020TB.odp XXX TO BE UPDATED)
    # A:= conversion factor from DVcal to electrons. [A]=Electrons/DVcal
    _avref09 = 11.3*e/ADC
    # B:= Offset, (noise) [B]=Electrons
    _bvref09 = 58.0*e
    def __init__(self,vref='0.8'):
        """The calibration conversor from DVCal to Electrons. Depending
        the Vref used, different linear factors are applied. See details
        at https://twiki.cern.ch/twiki/bin/view/RD53/BiasesNDacs53A 
        and [20200916_CNM3D_DESY2020TB.odp --> XXX PROVISIONAL]

        Parameters
        ----------
        vref: str
            The Vref used in [V]. Valid values are '0.8' (default)
            and '0.9'
        """
        if vref == '0.8':
            self.A = QDAC_Conversor._avref08
            self.B = QDAC_Conversor._bvref08
        elif vref == '0.9':
            print("Assume Vref=0.9 V for DVCal->electrons conversion")
            self.A = QDAC_Conversor._avref09
            self.B = QDAC_Conversor._bvref09
        else:
            raise RuntimeError('Invalid Vref value {}'.format(vref))
        self.err_evcal = 12.0*e

    def __call__(self,vcal):
        return (self.A*vcal+self.B)

# XXX -- Careful with this!! Only valid for the 3D mapping 
#        of FBK and also CNM (At least those from end of 2019 batch)
def roc_to_pixel_map_100by25_3D(col,row):
    """Map indices to convert from ROC -> PIXEL coordinates.
    Given the column and row from the ROC, the function returns
    the corresponding column and row of the pixel bump-bonded to
    that ROC channel.
    
    Parameters
    ----------
    col: int
    row: int

    Returns
    -------
    col_pixel,row_pixel
    
    Notes
    -----
    The function accepts numpy arrays as well.

    """
    return np.floor(col/2).astype(int),(2*row+(col%2))

def get_indices_for_100by25_3D(shape):
    """Creates a matrix with the ROC channel size, where each 
    element of the matrix `m[col,row]` contains a 2-tuple with 
    the corresponding column and row of the pixel bump-bonded to
    that ROC channel

    Parameter
    --------
    shape: (int,int)
        The number of columns and the number of rows of the ROC-chip

    Return
    ------
    numpy.ndarray: the matrix map to the PIXEL coordinates, being the
        shape of the new matrix: (shape[0],shape[1],2)        
    """
    nm = np.zeros((shape[0],shape[1],2),dtype=int)
    # Mapping ROC -> sensor
    for c in range(shape[0]):
        for r in range(shape[1]):
            col,row = roc_to_pixel_map_100by25_3D(c,r)
            nm[c][r] = [col,row]
    return nm

def convert_100by25_3D(roc_matrix):
    """Convert an array on the ROC coordinates into PIXEL coordinates.
    
    Parameter
    --------
    roc_matrix: numpy.ndarray on ROC coordinates

    Return
    ------ 
    numpy.ndarray: on pixel coordinates. The shape of the matrix will
        correspond to (roc_matrix.shape[0]*0.5,roc_matrix.shape[1]*2).
        The channels mapping is defined in the `roc_to_pixel_map_100by25_3D`.

    See also
    --------
    `get_indiced_for_100by25_3D`
    """
    # Obtain the map from the ROC coordinates to the pixel ones
    # just do it once...
    if not hasattr(convert_100by25_3D,'indices'):
        convert_100by25_3D.indices = get_indices_for_100by25_3D(roc_matrix.shape)
    # Prepare the format to feed the final matrix in pixel coordinates
    cmap = convert_100by25_3D.indices[:,:,0]
    rmap = convert_100by25_3D.indices[:,:,1]
    # The matrix converted from ROC to Pixel layout
    final = np.zeros((int(roc_matrix.shape[0]*0.5),int(roc_matrix.shape[1]*2)),dtype=roc_matrix.dtype)
    final[cmap,rmap]=roc_matrix

    return final

# dummy class to be used as container for the calibration curves
class CalibrationContainer(object):
    def __init__(self):
        self._array = None
        self._detected_all = None
        # The fit model 
        self._tot_response = None
        # The measured points
        self._graph = ROOT.TGraphErrors()
        # The calibration model parameters,
        # for ADC and electrons
        self._a = {}
        self._b = {}
        self._c = {}
        self._t = {}
        for wx in ['elec', 'vcal']: 
            self._a[wx] = None
            self._b[wx] = None
            self._c[wx] = None
            self._t[wx] = None

class tot_vcal_scan(object):
    """Get the information and manage the result table of
    a threshold scan. 
    XXX -- >> Is this true?
    Note that the pixels described in the file
    are converted into sensor layout. 
    XXX -- Currently only FBK 3D conversion is implemented XXX

    The file is ASCII file containing the outcome for each pixel
    at a given injection point. Each injection point is introduced by
    a header section such 
    ```
    Iteration <N> --- reg = <DeltaVCal step>
    ```
    being the <N> the number of the interation, and <DeltaVCal step>
    the used injection charge.

    Each line in between describes the pixel row (r) column (c), the 
    number of times the pixel was active (h), and the average ToT value
    received (a). The data doesn't containe the standard deviation of 
    this value (XXX --> TO BE REQUESTED or taken from the raw data)

    """
    def __init__(self,datfile,sensor_name,force_25by100,vref=None):
        """
        Parameters
        ----------
        datfile: str
        vref: str|None
            The Vref used for the DACs. Valid values are '0.9' 
            and '0.9' ('default')
            If None, it is going to use the content of the global `VREF`
        """
        # The Q(DVcal) conversion
        if not vref:
            self.evcal = QDAC_Conversor(VREF)
        else:
            self.evcal = QDAC_Conversor(vref)

        self._datfile = datfile
        # Read the files and store info
        with open(self._datfile) as f:
            lines = f.readlines()
        # Parse and check validity
        try: 
            # Extract the vcals steps
            index_vcal_list = list(map(lambda x: (x[0],int(x[1].split(' =')[-1])),
                filter(lambda x: x[1].find('--- reg = ') != -1,enumerate(lines))))
        except ValueError:
            RuntimeError('Invalid format for the calibration file "{}"'.format(self._datfile))

        self._sensor = sensor_name

        # r <ROW> c <COLZ> h <OCCUPANCY> a <TOT>
        ROW_I = 1; COL_I = 3; OCC_I = 5; TOT_I = 7

        # Get the metadata info of the scan
        # XXX hardcoded, no info present in the .dat files
        self._injections = 100 
        self._vcal_step  = len(index_vcal_list)
        # More extra info: extracted from the first iteration
        self._start_col  = int(lines[index_vcal_list[0][0]+1].split()[COL_I])
        self._stop_col   = int(lines[-1].split()[COL_I])+1
        self._start_row  = int(lines[index_vcal_list[0][0]+1].split()[ROW_I])
        self._stop_row   = int(lines[-1].split()[ROW_I])+1
        
        # built a relative lookup table where to find the relative index 
        # given a col-row. The index should be called adding it to the `index_vcal_list`
        # NOT using this --> uses references!!!
        # lookup = dict.fromkeys(range(self._start_col,self._stop_col),
        #                    dict.fromkeys(range(self._start_row,self._stop_row)))
        lookup = { key : {row: None for row in range(self._start_row, self._stop_row)} 
                for key in range(self._start_col,self._stop_col) }
        # Just look at the first iteration: note it is stored as ROW-- COL
        for i,lin in enumerate(lines[1:index_vcal_list[1][0]]):
            lookup[int(lin.split()[COL_I])][int(lin.split()[ROW_I])] = i+1

        # Get the array of injected vcal and the ToT
        self._vcal_injected = np.array(list(map(lambda _x: _x[1],index_vcal_list)),dtype=int)
        # Get in electrons using the calibration curve for vcal
        self._elec_injected = self.evcal(self._vcal_injected)
        self._tot_values    = np.array(list(range(nTOT)),dtype=int)
        
        # XXX - Get the measurements: built a matrix of 100 injections although so far they
        # are not present in the .dat files. Just filling it with the same value all of them
        # Create the matrix
        self._measurements = np.zeros(shape=(self._stop_col,self._stop_row,self._vcal_step,2))
        with Bar("[INFO] Extracting info from calibration file",
                max=(self._stop_col-self._start_col)*(self._stop_row-self._start_row),
                suffix='%(percent)d%%') as bar:
            for col in range(self._start_col, self._stop_col):
                for row in range(self._start_row, self._stop_row):
                    # For all vcal
                    for i,(line_start, vcal) in enumerate(index_vcal_list):
                        # The occuppancy
                        try: 
                            self._measurements[col,row][i][0] = lines[line_start+lookup[col][row]].split()[OCC_I]
                        except TypeError:
                            # The pixel was not calibrated --> lookup[col][row] = None
                            bar.next()
                            continue
                        # The ToT average
                        self._measurements[col,row][i][1] = lines[line_start+lookup[col][row]].split()[TOT_I]
                    bar.next()

        ## If 25x100 
        if force_25by100: 
            self._start_col,_  = roc_to_pixel_map_100by25_3D(self._start_col,0)
            self._stop_col,_   = roc_to_pixel_map_100by25_3D(self._stop_col,0)
            _,self._start_row  = roc_to_pixel_map_100by25_3D(0,self._start_row)
            _,self._stop_row   = roc_to_pixel_map_100by25_3D(0,self._stop_row)
            # And all other maps
            prov_aux = np.zeros(shape=(self._stop_col,self._stop_row,self._measurements.shape[2],self._measurements.shape[3]))
            with Bar("All data is being converted from ROC -> PIXEL coordinates",
                    max=(self._measurements.shape[2]*self._measurements.shape[3]),
                    suffix='%(percent)d%%') as bar:
                for dvcal in range(self._measurements.shape[2]):
                    for totbin in range(self._measurements.shape[3]):
                        prov_aux[:,:,dvcal,totbin] = convert_100by25_3D(self._measurements[:,:,dvcal,totbin])
                        bar.next()
                self._measurements = prov_aux

        ## Memoizers for the extracted data for the pixels
        self._pixels_meas = {}
        self._cal_curves  = {}

        # Some messages to show once finished
        self._messages = []

    @property
    def start_col(self):
        """The first column of the measured pixels
        """
        return self._start_col
    
    @property
    def stop_col(self):
        """The last column of the measured pixels
        """
        return self._stop_col

    @property
    def start_row(self):
        """The first rowof the measured pixels
        """
        return self._start_row
    
    @property
    def stop_row(self):
        """The last row of the measured pixels
        """
        return self._stop_row

    def print_messages(self):
        if len(self._messages) > 0:
            print("Captured messages:")
            for m in self._messages:
                print(m)
            # initialize again
            self._messages = []


    def close_file(self):
        """XXX
        """
        self._h5.close()
    
    def has_calibration_curve(self,col,row):
        """Whether or not the obtained curve for ToT vs. elec/VCal is filled
        """
        return self._cal_curves[(col,row)]._present

    def get_measurements_matrix(self,pix_col,pix_row):
        """Extract the measurement matrix for the given pixel. 
        How many times a ToT was measured when a Vcal was injected: [VCal,ToT]
        Note that the total sum of measured ToT for a given VCal must be
        equal or lower than n_injections: sum([n0,n1,n2,...,n_nTOT]) <= n_injected

        XXX :: THIS IS NOT LIKE THAT in Ph2_ACF!!

        Parameters
        ----------
        pix_col: int
        pix_row: int

        Return
        ------
        pixel: np.array (shape=(Vcal_max-Vcal_being)/Vcal step,nTOT)
            How many times a ToT was measured for a given Vcal (over the
            total n_injections)
        """
        assert isinstance(pix_col,int) and isinstance(pix_row,int),"Arguments must be integers"
        if (pix_col,pix_row) in self._pixels_meas:
            return self._pixels_meas[(pix_col,pix_row)]
        # Find if the pixel is outside the scan limits
        if self._start_col > pix_col > self._stop_col:
            # Empty (FIXME: Maybe a None)
            self._pixels_meas[(pix_col,pix_row)] = np.zeros(shape=(self._vcal_step,nTOT),dtype=int)
        else:
            # Obtain the matrix for all the Vcal steps and measured ToT
            self._pixels_meas[(pix_col,pix_row)] = self._measurements[pix_col,pix_row,:,:]

        return self._pixels_meas[(pix_col,pix_row)]

    def calibration_curve(self,col,row):
        """Obtain the calibration curve for the given pixel, and perform 
        the fit to extract the slope and offset. The information is store in
        a `CalibrationContainer` object (the array with all measurements, 
        the graph with averaged measurements, and the slope and offset of
        the fit function

        Parameters
        ----------
        col: int
        row: int

        Return
        ------
        list((float,float)) (len=n_injection)
            The list contains the average ToT measured (and its dispersion)
            for each VCal injected
        """
        assert isinstance(col,int) and isinstance(row,int),"Arguments must be integers"
        if (col,row) in self._cal_curves:
            return self._cal_curves[(col,row)]._array

        # Create the Calibration Container (
        self._cal_curves[(col,row)] = CalibrationContainer()

        # Find if the pixel is outside the scan limits
        if self._start_col > col > self._stop_col:
            # Empty (FIXME: Maybe a None)
            self._cal_curves[(col,row)]._array = np.zeros(shape=self._vcal_injected.shape[0],dtype=int)
            return self._cal_curves[(col,row)]._array

        # For a given pixel [col,row, :, : ], the matrix describes how many times 
        # a ToT was measured for a given VCal value (see `get_measurements_matrix`)
        m = self.get_measurements_matrix(col,row)
        # And evalute all the VCal 
        self._cal_curves[(col,row)]._array = np.zeros(shape=(self._vcal_injected.shape[0],2))
        self._cal_curves[(col,row)]._detected_all = np.zeros(shape=(self._vcal_injected.shape[0]),dtype=bool)
        for i,vcal in enumerate(self._vcal_injected):
            average_tot = m[i][1]
            # XXX -- Being proportional to the number of injection and activations??? Binomial distr.
            std_tot     = np.sqrt(m[i][0]*(1.0-m[i][0]/float(self._injections)))
            self._cal_curves[(col,row)]._array[i] = average_tot,std_tot
            # Mark if was detected all the injections
            self._cal_curves[(col,row)]._detected_all[i] = (m[i][0] >= self._injections)
            # XXX -- We can extract this info as well in a dedicated function, just
            #        to obtain the cross-talk effect...
            if m[i][0] > self._injections:
                self._messages.append('Cross-talk indication: Pixel {} {}, '\
                        'sum of all entries > total injection'.format(col,row))
        # The curve to extract the slope and offset
        # Obtain the graphs and extract the calibration constant
        # get a shorter name
        gr = self._cal_curves[(col,row)]._graph
        x = self._vcal_injected
        gr.SetName('cal_curve_{}_{}'.format(col,row))
        gr.SetMarkerStyle(20)
        gr.SetMarkerSize(1.)
        # Take the diference between points as uncertainty in x
        dx = x[1]-x[0]
        i_gr = 0
        for i,(val,val_err) in enumerate(self._cal_curves[(col,row)]._array):
            # Just include those points which detected all the injections
            if not self._cal_curves[(col,row)]._detected_all[i]:
                continue
            gr.SetPoint(i_gr,x[i],val)
            gr.SetPointError(i_gr,dx,val_err)
            i_gr+=1
        # Fit a function defined at  [1] to extract the calibration constant. 
        # [1] https://cds.cern.ch/record/2649493/files/CLICdp-Note-2018-008.pdf
        # 
        # Given that the ToT is a discrete quantity, n-Heaviside functions per 
        # each ToT can the response. Actualy n-activation functions
        # with their offset in the ToT = N, and the middle point giving the average
        # value to change of ToT
        try:
            # XXX - TBD
            xmin_ind = max(0,np.where(self._cal_curves[(col,row)]._array > 0)[0][0]-1)
        except IndexError:
            # This is a full zero array, just mark it. Meaning no ToT was measured
            # for all the vcal steps
            self._messages.append('Not working: Pixel {} {}, no ToT was measured '\
                    'for all the VCal steps (each step n-injections)'.format(col,row))
            self._cal_curves[(col,row)]._present = False
            return self._cal_curves[(col,row)]._array

        # Assume the threshold: take from the midpoint between the last point with 0 response
        # and the first one with 100% injections 
        try:
            last_point_0    = np.where(self._cal_curves[(col,row)]._array[:,0] == 0)[0][-1]
        except IndexError:
            last_point_0 = 0
            self._messages.append('Probably noise: Pixel {} {}, all VCal steps were '\
                    'able to measure all the injection, meaning no threshold was found'.format(col,row))
        try:
            first_point_100 = np.where(self._cal_curves[(col,row)]._detected_all)[0][0]
        except IndexError:
            # Not all of them, let's tag it as failed pixel
            self._messages.append('Probably inefficient: Pixel {} {}, no VCal steps were '\
                    'able to measure all the injections, meaning injected charge is lost'.format(col,row))
            self._cal_curves[(col,row)]._present = False
            return self._cal_curves[(col,row)]._array
        midpoint = np.mean([x[first_point_100],x[last_point_0]])
        std_midpoint = np.std([x[first_point_100],x[last_point_0]])

        # Check if there is saturation, and exclude those points from the fit (otherwise, the fit model
        # below needs to be changed) 
        try:
            # XXX -- TODO Check more than just the first point, to be sure it's not an outlier
            first_point_saturation = np.where(self._cal_curves[(col,row)]._array[:,0] == TOT_BIT_SATURATION)[0][0]
            maxpoint = x[first_point_saturation]
        except IndexError:
            maxpoint = x.max()*1.1
        
        # Response of the Tot when injecting charge:
        # https://cds.cern.ch/record/2649493/files/CLICdp-Note-2018-008.pdf
        # 
        # The ToT response model a*vcal+b-c/(vcal-t)
        self._cal_curves[(col,row)]._tot_response = p1 = \
                ROOT.TF1('tot_vcal_{}_{}'.format(col,row),'[0]*x+[1]-[2]/(x-[3])',midpoint, maxpoint)
        # Set the threshold from the midpoint between the last injection with 
        # no response at all and the first one with all injections with ToT measured
        # Assigned as error the distance 
        p1.SetParameter(3,midpoint)
        # -- Fit stability is complicated if not fixed this parameter
        p1.SetParLimits(3,midpoint,midpoint)
        #Initialize the others
        # -- The slope of the linear part
        p1.SetParameter(0,0.001)
        p1.SetParLimits(0,0,1)
        # -- The offset of the linear part
        p1.SetParameter(1,0)
        # -- The curvature of the non-linear part
        p1.SetParameter(2,40)
        p1.SetParLimits(2,20,1e3)
        # Convergence:  -- 
        st=gr.Fit(p1,'RQ')
        
        # The slope and offset with its errors (are they those, should I include any other?)
        self._cal_curves[(col,row)]._present = True

        # Assigned as error the distance (do it after the fit, otherwise will be 0)
        p1.SetParError(3,std_midpoint)
        # Assuring the internal function as well
        try:
            gr.GetListOfFunctions()[0].SetParError(3,std_midpoint)
        except AttributeError:
            # XXX - Should I do that?? 
            self._messages('Fit problem: Pixel {} {}, calibration curve was not fitted'.format(col,row))
        
        # Store explicitely the fitted parameters of the ToT response model
        avcal = self._cal_curves[(col,row)]._a['vcal'] = (p1.GetParameter(0),p1.GetParError(0))
        bvcal = self._cal_curves[(col,row)]._b['vcal'] = (p1.GetParameter(1),p1.GetParError(1))
        cvcal = self._cal_curves[(col,row)]._c['vcal'] = (p1.GetParameter(2),p1.GetParError(2))
        tvcal = self._cal_curves[(col,row)]._t['vcal'] = (p1.GetParameter(3),p1.GetParError(3))

        # Converting calibration curve from vcal to electrons
        # 
        #   electrons= A*vcal+B (the evcal function)
        # 
        # The ToT response model a*vcal+b-c/(vcal-t), in electrons become
        # 
        #    a_elec:= a/A       [ToT/electrons]
        #    b_elec:= b - aB/A  [ToT]
        #    c_elec:= c*A       [electrons]
        #    t_elec:= B+t*A     [electrons]
        self._cal_curves[(col,row)]._a['elec'] = (avcal[0]/self.evcal.A,avcal[1]/self.evcal.A)
        self._cal_curves[(col,row)]._b['elec'] = (bvcal[0]-avcal[0]*self.evcal.B/self.evcal.A,
                np.sqrt(bvcal[1]**2.+(self.evcal.B/self.evcal.A*bvcal[1])**2.))
        self._cal_curves[(col,row)]._c['elec'] = (cvcal[0],cvcal[1]*self.evcal.A)
        self._cal_curves[(col,row)]._t['elec'] = (tvcal[0]*self.evcal.A+self.evcal.B,tvcal[1]*self.evcal.A)
        
        return self._cal_curves[(col,row)]._array

    def store_calibration_curves(self,fname=None):
        """Store all the calibration curves graphs into a root file

        Parameters
        ----------
        fname: str|None
            The name of the root output file
        """
        if fname:
            rootfname = fname
        else:
            rootfname = '{}_{}_calibration_constants.root'.format(
                    self._sensor,datetime.now().strftime('%Y%m%d_%H%M%S'))
        f = ROOT.TFile.Open(rootfname,'RECREATE')

        with Bar("Store calibration curves",
                max=(self.stop_col-self.start_col)*(self.stop_row-self.start_row),
                suffix='%(percent)d%%') as bar:
            for ccont in self._cal_curves.values():
                ccont._graph.Write()
                bar.next()
            # Actually writing to the file
            f.Close()
    

    def plot_calibration(self,col,row,fmt='png',ghistos=None):
        """Plot the calibration curve for the pixel

        Parameters
        ----------
        col: int
            Pixel column
        row: int
            Pixel row
        fmt: str
            The plot suffix [Default: png]
        ghistos: list(ROOT.TH1F)|None
            If present the global histograms for the parameters. 
            Note, the a (slope) and b (offset) parameters are 
            expected to store the inverse of them            
        """
        style=pl.set_sifca_style(squared=True,stat_off=True)
        style.cd()
        batch=ROOT.gROOT.IsBatch()
        ROOT.gROOT.SetBatch()
        # Get the curves and build the average function
        cc = self.calibration_curve(col,row)
        # Check if the pixel is calibrated
        if not self.has_calibration_curve(col,row):
            return
        
        # The x-axes
        xvcal = self._vcal_injected
        xvcal_title = '#DeltaV_{cal}'
        
        # Prepare the plot. Fixe the frame
        c0 = ROOT.TCanvas()
        xmin,xmax = xvcal.min()*0.9,xvcal.max()*1.1
        ymin,ymax = 0.0,np.max(cc[:,0]+cc[:,1])*1.15
        h = c0.DrawFrame(xmin,ymin,xmax,ymax)
        h.SetTitle(';{};<ToT> (over {}-injections)'.format(xvcal_title,self._injections))
        # Prepare the two axis
        elec_axis = prepare_electron_axis(xmin,xmax,ymin,h,style)
        # Plot all elements
        h.Draw()
        elec_axis.Draw()
        
        # Function to print both vcal and electron values
        vstr1f = lambda valerr:"{"+'{:.1f}#pm{:.1f}'.format(*valerr)+"}"
        vstr0f = lambda valerr:"{"+'{:.0f}#pm{:.0f}'.format(*valerr)+"}"
        both_units = "#dot^{#DeltaV_{cal}}_{Elecs.}"
        
        # Extract some parameters to print them out
        # a and threshold -------------------------
        slope = self._cal_curves[(col,row)]._a['vcal']
        inv_slope = (slope[0]**-1,slope[0]**-2*slope[1])
        slope_elec = self._cal_curves[(col,row)]._a['elec']
        inv_slope_elec = (slope_elec[0]**-1,slope_elec[0]**-2*slope_elec[1])
        
        threshold = self._cal_curves[(col,row)]._t['vcal']
        threshold_elec = self._cal_curves[(col,row)]._t['elec']
        # Print the fit output
        ypos= 0.80
        xpos=0.45
        lp1=ROOT.TLatex()
        lp1.DrawLatexNDC(0.1,0.92,'Pixel: {}_{} {}_{}'.format(col,'{col}',row,'{row}'))
        lp1.DrawLatexNDC(xpos,ypos,'1/a=^{}_{} #left[{}/ToT#right]'.format(vstr0f(inv_slope),vstr0f(inv_slope_elec),both_units))
        #lp1.DrawLatexNDC(xpos,ypos-1*0.06,'1/b=^{}_{} #left[{}#right]'.format(vstr1f(inv_offset),vstr1f(inv_offset_elec),both_units))
        lp1.DrawLatexNDC(xpos,ypos-0.06,'x_{}=^{}_{} #left[{}#right]'.format('{thr}',vstr0f(threshold),vstr0f(threshold_elec),both_units))
        lp1.DrawLatexNDC(xpos-0.3,ypos,'ax + b - #frac{}{}'.format('{c}','{x-x_{trh}}'))

        # Threshold and uncertainty
        thr_err = ROOT.TBox(threshold[0]-threshold[1],0,threshold[0]+threshold[1],h.GetMaximum())
        #thr_err.SetFillColor(ROOT.kGray)
        thr_err.SetFillColorAlpha(ROOT.kGray,0.5)
        thr_err.Draw('F')

        thr = ROOT.TLine(threshold[0],0,threshold[0],h.GetMaximum())
        thr.SetLineWidth(2)
        thr.SetLineColor(ROOT.kRed+3)
        thr.Draw()
        # Also a label
        lp1.DrawLatex((threshold[0]+threshold[1])*1.05,h.GetMaximum()*0.95,'x_{thr}')

        # And the curve
        self._cal_curves[(col,row)]._graph.Draw('PE1')

        c0.SaveAs('{}_calibration_tot_pixel_{}_{}.{}'.format(self._sensor,col,row,fmt))
        
        # Store the constants if required
        # XXX FIXME -- Now needed two axis as well ??
        if ghistos:
            curvature = self._cal_curves[(col,row)]._c['vcal']
            curvature_elec = self._cal_curves[(col,row)]._c['elec']
            # The histograms are expected to be ordered like the parameters
            for i,(v,verr) in enumerate([inv_slope,inv_offset,curvature,threshold]):
                ghistos[i].Fill(v)

        # Return same ROOT conditions
        ROOT.gROOT.SetBatch(batch)
        ROOT.gROOT.GetListOfCanvases().Delete()


    def dump_calibration_file(self):
        """Create a file with the extracted a,b,c and t parameters
        for the calibration curves, fit to the response function: ax+b-c/(x-t)
        in ADC and electrons units. The electrons units have been converted
        by using the `evcal` function

        Return
        ------
        filename: (str,str)
            The names of the created files (vcal and electrons)
        """
        # XXX - Need split elec/vcal or just create it in the same matrix
        #       and therefore, used with the same class
        # XXX 

        # First store it in an numpy array (a table maybe?)
        # Obtain the chip size from the tdac mask
        # The matrix is 4 times the ROC matrix, one for each parameter: a,b,c and t
        # Be sure the evaluation and the fitting is done,
        calibration_constants = np.zeros(shape=(4,self._stop_col,self._stop_row))
        calibration_constants_elec = np.zeros(shape=(4,self._stop_col,self._stop_row))
        not_calibrated_pixels = []
        with Bar("Obtaining calibration curves",
                max=(self.stop_col-self.start_col)*(self.stop_row-self.start_row),
                suffix='%(percent)d%%') as bar:
            for col in range(self.start_col,self.stop_col):
                for row in range(self.start_row,self.stop_row):
                    # Be sure is an active pixel
                    #if (col,row) not in self._measurements:
                    #    bar.next()
                    #    continue
                    _ = self.calibration_curve(col,row)
                    # Now the constants are present, 
                    try: 
                        calibration_constants[0][col,row] = self._cal_curves[(col,row)]._a['vcal'][0]
                        calibration_constants_elec[0][col,row] = self._cal_curves[(col,row)]._a['elec'][0]
                    except TypeError:
                        not_calibrated_pixels.append((col,row))
                        bar.next()
                        continue
                    calibration_constants[1][col,row] = self._cal_curves[(col,row)]._b['vcal'][0]
                    calibration_constants_elec[1][col,row] = self._cal_curves[(col,row)]._b['elec'][0]
                    calibration_constants[2][col,row] = self._cal_curves[(col,row)]._c['vcal'][0]
                    calibration_constants_elec[2][col,row] = self._cal_curves[(col,row)]._c['elec'][0]
                    calibration_constants[3][col,row] = self._cal_curves[(col,row)]._t['vcal'][0]
                    calibration_constants_elec[3][col,row] = self._cal_curves[(col,row)]._t['elec'][0]
                    bar.next()
        if len(not_calibrated_pixels) > 0:
            print("List (col,row) of non-calibrated ACTIVE pixels (please take a look):")
            print(str(not_calibrated_pixels).replace('[','').replace(']',''))

        # Ready to create the file
        filenames = []
        for calmat,which in [ (calibration_constants,'vcal'),(calibration_constants_elec,'elec') ]:
            filename = '{}_{}_calibration_constants_{}.npy'.format(self._sensor,datetime.now().strftime('%Y%m%d_%H%M%S'),which)
            np.save(filename,calmat,fix_imports=True)
            filenames.append(filename)

        return filenames[0],filenames[1]
    

# XXX - Need split elec/vcal or just create it in the same matrix
#       and therefore, used with the same class
class apply_calibration(object):
    """Class encapsulating the conversion from ToT to vcal or elec.
    It relies on the `tot_vcal_scan` class to create the calibration constants
    from a threshold scan file, after using the `dump_calibration_file` 
    method to create the calibration file to use.
    
    The file contains the constants used for the fit of the ToT-(vcal/elec)
    curves: 
        :math: f_{ToT}(x)= a x+b -\frac{c}{x-t}

    Therefore in order to be used, i.e., to convert ToT into vcal/elec:
        :math: f^{-1}_{ToT}(x) = \frac{at+x-b+\sqrt{(b+at-x)^2+4ac}}{2a}
    """
    def __init__(self,calfile,mask_large_slope=False):
        """
        Parameters
        ----------
        calfile: str
            Calibration file created by the `tot_vcal_scan` class. 
        mask_large_slope: bool
            Whether to remove (mask) pixels with a large fitted slope, 
            larger than 10kelectrons per ToT (make no sense and indicates
            the fit failed). 
            If this is false, instead of masking it will asign to the 
            nonsense pixel an average value 

        """
        # function behaviour dependent on whether the mask is applied or not
        self.is_pixel_masked = None

        pre_masked = np.load(calfile)
        if mask_large_slope:
            self.is_pixel_masked = self._is_pixel_masked
            # The parameters array
            self._ctes = np.ma.zeros(shape=pre_masked.shape)
            # Mask those pixels with non-sense slopes
            self._ctes[0] =  np.ma.array(pre_masked[0],mask=(pre_masked[0] < 1e-5))
            # Propagate mask to all parameters
            for i in range(1,len(pre_masked)):
                self._ctes[i] = np.ma.array(pre_masked[i],mask=self._ctes[0].mask)
        else:
            self.is_pixel_masked = self._is_always_valid
            # The parameters array
            self._ctes = np.ma.zeros(shape=pre_masked.shape)
            # Get those pixels with non-sense slopes
            slope_properval = np.ma.array(pre_masked[0],mask=((pre_masked[0] < 1e-5) & (pre_masked[0] > 0.0) ))
            # Propagate mask to all parameters
            for i in range(0,len(pre_masked)):
                cte_properval = np.ma.array(pre_masked[i],mask=slope_properval.mask)
                # Subtitute the masked values by the mean of the unmasked
                #self._ctes[i] = cte_properval.filled(cte_properval.mean())
                # Evaluate the median, instead of the mean
                self._ctes[i] = cte_properval.filled(np.ma.median(cte_properval))
        
        # Assumes standard name convention
        self._sensor = os.path.basename(calfile).split('_')[0]
        self._x      = os.path.basename(calfile).split('_')[5].split('.')[0]
        # Only use the 'vcal', the conversion will be done here
        if self._x != 'elec' and self._x != 'vcal':
            raise RuntimeError('Please use the standarized convention for the file name')
        if self._x == 'elec':
            print('Use the "vcal" version, the "ecal" will be deprecated soon.')
            # XXX -- raise RuntimeError

        # A memoization for not repeating calculations
        self._response_back = lambda ToT : (self.a*self.t + ToT - self.b - \
                np.sqrt((self.b+self.a*self.t-ToT)**2.0-4.0*self.a*(ToT*self.t-self.b*self.t-self.c)))/(2.0*self.a)

    @property
    def a(self):
        """The slope
        """
        return self._ctes[0]

    @property
    def b(self):
        """The offset
        """
        return self._ctes[1]

    @property
    def c(self):
        """The rising curvature
        """
        return self._ctes[2]

    @property
    def t(self):
        """The threshold
        """
        return self._ctes[3]

    def __call__(self,col,row,ToT):
        """The calibration response

        Parameters
        ----------
        col: int
            Pixel column
        row: int
            Pixel row 
        ToT: float
            The time over threshold measurement

        Return
        ------
        The measured charge in vcal/elec units
        """
        return self._response_back(ToT)[col,row]

    def _is_always_valid(self,col,row):
        """ Dummy function returning always False, in 
        order to be compatible with the `is_pixel_masked` 
        expected behaviour function
        """
        
        return False

    def _is_pixel_masked(self,col,row):
        """Whether a col,row is masked, therefore there is
        no calibration curve available
        
        Parameters
        ----------
        col: int
            Pixel column
        row: int
            Pixel row 

        Return
        ------
        bool: True if is a masked pixel

        Raise
        -----
        AttributeError if this is not a masked Array
        """
        # FIXME: assert? -- it's always a masked Array, right?
        return bool(self._ctes[0].mask[col,row])

    def dump_text_file(self,filename,a_cutoff=1e-5):
        """FIXME
        """
        # FIXME: TO BE REMOVE OR SEND to the tot_vcal class
        # Just keep those entry with sensible values (a slope lower than
        # 1/a= 10000 something/ToT (i.e. a = 1e-5) makes no sense..
        # evn lower will make no sense
        # FIXME: DOCUMENT cutoff (which is this thing
        ascii_lines = []
        for (col,row) in np.argwhere(np.abs(self.a) > a_cutoff):
            # col row a b c t
            ascii_lines.append('{} {} {} {} {} {}\n'.format(col,row,
                self.a[col,row],self.b[col,row],self.c[col,row],self.t[col,row]))

        with open(filename,'w') as f:
            f.writelines(ascii_lines)

class calibration_plotter(apply_calibration):
    """Class used to plot the calibration curves
    """
    def __init__(self,calcfile,rootfile=None):
        """
        Parameters
        ----------
        calfile: str
            Calibration file created by the `tot_vcal_scan` class. 
        rootfile: str
            The equivalent rootfile, if None extracts the root file
            name from calcfile, substituting `.npy` by `.root`
        """
        super(calibration_plotter,self).__init__(calcfile)
        # And the equivalent ROOT file
        if rootfile is not None:
            self._rootfile = ROOT.TFile.Open(rootfile)
        else:
            self._rootfile = ROOT.TFile.Open(calcfile.replace('_vcal.npy','.root').replace('_elec.npy','.root'))
    
    def plot_all_calibration_curves(self):
        """Plot the calibration curves and measured values for all pixels
        in an unique canvas (one per each)

        Return
        ------
        str,str: the name of the plot files (measurement, fitted)
        """
        style=pl.set_sifca_style(squared=True,stat_off=True)
        style.cd()
        ROOT.gROOT.SetBatch()

        # Extract all curve to obtain limits on the histos
        calcurves = []
        measurements = []
        xmin = 0
        ymin = 0 
        ymax = 14
        # Some 
        nkeys = len(self._rootfile.GetListOfKeys())
        with Bar("Extracting pixels calibration curves",max=nkeys,suffix='%(percent)d%%') as bar:
            for key in filter(lambda key: key.GetClassName().find('TGraph') == 0,self._rootfile.GetListOfKeys()):
                gname=key.GetName()
                # The measurements (means of each injection step)
                mgr = self._rootfile.Get(gname)
                # The calibration curve
                try:
                    calcurves.append(mgr.GetListOfFunctions()[0])
                except IndexError:
                    # Calibration curve not present, fill it with None
                    calcurves.append(None)
                # Fill the array of measurements points already
                n = mgr.GetN()
                ary = np.zeros((n,2))
                try:
                    ary[:,0]=mgr.GetX()
                    ary[:,1]=mgr.GetY()
                except ReferenceError:
                    # Covering the case no point was actually found in the graph...
                    # Not needed in python3
                    pass
                # Be sure to discard those dummy values in the graph with no measured values)
                measurements.append(ary[ary[:,0]>1e-1])
                bar.next()
        # Found limits for the histos
        xmin = measurements[0].min()*0.97
        xmax = measurements[1].max()*1.03
        # Binning like the number of points measured (a 40% more)
        nxbins = int(len(measurements[0])*1.4)
        title = "#DeltaV_{cal}"
        hmeas = ROOT.TH2F('meas',';{};Measured Charge [ToT];# pixels'.format(title),\
                nxbins,xmin,xmax,100,ymin,ymax)
        
        # For the curves, lets create a more dense array and doubling the range 
        # Fixing xmax to 1800 DVcal
        xmax = 1800
        xcurves = np.linspace(xmin,xmax,num=100)
        dx = xcurves[1]-xcurves[0]
        hcurv = ROOT.TH2F('tot',';{};Measured Charge [ToT];# pixels'.format(title),\
                len(xcurves),xcurves[0]-dx*0.5,xcurves[-1]+dx*0.5,100,ymin,ymax)
        # Fill the histos: for all pixels
        with Bar("Populating histograms               ",
                max=len(calcurves),suffix='%(percent)d%%') as bar:
            for ipx,cc in enumerate(calcurves):
                xyarr = measurements[ipx]
                for i,(x,y) in enumerate(xyarr):
                    _ = hmeas.Fill(x,y)
                # Not present
                if cc is None:
                    bar.next()
                    continue
                # Create more points, at least 2xtims range
                current_threshold = cc.GetParameter(3)
                # Be careful not to add any point before the asymptot (at threshold)
                for xc in xcurves[xcurves>current_threshold]:
                    _ = hcurv.Fill(xc,cc(xc))
                bar.next()
        c = ROOT.TCanvas()
        c.SetLogz()
        # Prepare the electron axis
        xmin,xmax = hmeas.GetXaxis().GetBinLowEdge(1),hmeas.GetXaxis().GetBinLowEdge(hmeas.GetNbinsX()+1)
        ymin = hmeas.GetYaxis().GetBinLowEdge(1)
        elec_axis = prepare_electron_axis(xmin,xmax,ymin,hmeas,style)
        hmeas.Draw('COLZ')
        elec_axis.Draw()
        mfname = '{}_calibration_measurements_curve_allpixels.png'.format(self._sensor)
        c.SaveAs(mfname)
        # Same for hcurv
        hcurv.Draw('COLZ')
        xmin,xmax = hcurv.GetXaxis().GetBinLowEdge(1),hcurv.GetXaxis().GetBinLowEdge(hcurv.GetNbinsX()+1)
        ymin = hcurv.GetYaxis().GetBinLowEdge(1)
        elec_axis = prepare_electron_axis(xmin,xmax,ymin,hcurv,style)
        #hcurv.Draw('COLZ')
        elec_axis.Draw()
        cfname='{}_calibration_curves_curve_allpixels.png'.format(self._sensor)
        c.SaveAs(cfname)

        return mfname,cfname

    def plot_distributions(self):
        """Plot the distribution of the calibration constant 
        for all pixels
        """
        style=pl.set_sifca_style(squared=True,stat_off=True)
        style.cd()
        style.SetOptStat(111111)
        ROOT.gROOT.SetBatch()

        # XXX I need a enable pixel mask to skip not-enable pixel!!

        # The axis limits depending the unit
        if self._x == 'vcal':
            xs = 0,300
            xc = 20,100
            xt = 40,300
            xunit='#DeltaV_{cal}'
        elif self._x == 'elec':
            xs = 0,3000
            xc = 20,100
            xt = 400,3000
            xunit = 'Electrons'

        
        hslope = ROOT.TH1F('slope',';(inverse) linear slope [{}/ToT];# pixels'.format(xunit),200,xs[0],xs[1])
        hoffset= ROOT.TH1F('offset',';offset [ToT];# pixels',200,-4,4)
        hcurvature= ROOT.TH1F('curvature',';c [ToT {}];# pixels'.format(xunit),200,xc[0],xc[1])
        hthreshold= ROOT.TH1F('threshold',';threshold [{}];# pixels'.format(xunit),100,xt[0],xt[1])
        # Keep the same order than the parameters in the function
        ghistos = [hslope,hoffset,hcurvature,hthreshold]
        functs  = [lambda x: x**-1,lambda x: x,lambda x: x,lambda x: x]
        # Change loop behavior depending masked or not
        if type(self._ctes[0]) == np.ma.MaskedArray:
            looping = lambda the_array: the_array.compressed()
        else:
            looping = lambda the_array: the_array
        for i,h in enumerate(ghistos):
            for col_array in self._ctes[i]:
                for val in looping(col_array):
                    # XXX --- PATCH!!!
                    if val == 0:
                        continue
                    # XXX --- PATCH!!!
                    try:
                        final_val = functs[i](val)
                    except ZeroDivisionError:
                        final_val = 0.0
                    # Ignore 0-values from masked pixels
                    h.Fill(final_val)
            c = ROOT.TCanvas()
            h.Draw()
            c.SaveAs('{}_calibration_ctes_allpixels_{}_{}.png'.format(self._sensor,h.GetName(),self._x))
            c.SetLogy()
            h.Draw()
            c.SaveAs('{}_calibration_ctes_allpixels_{}_{}_log.png'.format(self._sensor,h.GetName(),self._x))


def prepare_electron_axis(xmin,xmax,ymin,frame,style,
        title="Electrons          ",conversion=QDAC_Conversor(VREF)):
    """Prepare all elements to create the electron axis along the DVcal one

    Parameters
    ----------
    (xmin,xmax): (float,float)
        The range in the Vcal original units
    ymin: float
        The y-position where to place the axis 
    frame: ROOT.TH1
        The frame
    style: ROOT.TStyle

    Returns
    -------
    ROOT.TGaxis
    """
    # Move 
    frame.SetTitleOffset(1.5)
    elec_axis = ROOT.TGaxis(xmin,ymin,xmax,ymin,conversion(xmin),conversion(xmax),405,"-")
    elec_axis.SetNoExponent(True)
    #color_axis=ROOT.kTeal+3
    color_axis=ROOT.kGray+2
    elec_axis.SetLabelColor(color_axis)
    elec_axis.SetTitleColor(color_axis)
    elec_axis.SetLineColor(color_axis)
    elec_axis.SetLabelFont(style.GetLabelFont())
    elec_axis.SetLabelSize(style.GetLabelSize())
    elec_axis.SetTitleFont(style.GetTitleFont())
    elec_axis.SetTitleSize(style.GetTitleSize())
    elec_axis.SetLabelOffset(-0.08)
    elec_axis.SetTitle(title)
    elec_axis.SetTitleOffset(-1.45)

    return elec_axis


@click.group()
def cli():
    pass

@cli.command(name='convert')
@click.argument('ifile')
@click.option('-p','--pixel-plot', nargs=2, metavar='COL ROW', help='Create the calibration curve for the pixel')
@click.option('-s','--sensor-name', default='DUMMYNAME', help='Assign a name to the sensor [default: DUMMY]')
@click.option('-f','--force-25by100', is_flag=True, help='The sensors is going to be considered a 25x100')
def convert_command(ifile, pixel_plot, sensor_name, force_25by100):
    """Create the calibration constants file to feed the EventLoaderEUDAQ module
    from Corryvreckan. The calibration file is obtained from the Ph2_ACF DAQ.

    ifile: Input calibration file in ASCII format (from the GAIN calibration in Ph2_ACF)
    """
    # Get the file threshold scan
    a = tot_vcal_scan(ifile,sensor_name=sensor_name, force_25by100=force_25by100)
    # Set Minuit2 as minimizer (better and faster)
    #ROOT.Math.MinimizerOptions.SetDefaultMinimizer("Minuit2")
    #ROOT.Math.MinimizerOptions.SetDefaultPrintLevel(0)

    ##filename = a.dump_calibration_text_file()
    filenames = a.dump_calibration_file()
    print("Created calibration files for {}: {}".format(a._sensor,filenames))
    a.store_calibration_curves()
    if pixel_plot:
        a.plot_calibration(pixel_plot[0],pixel_plot[1])
    ## And Create the text file
    print("Created text calibration files for {}:".format(a._sensor))
    for f in filenames:
        filename_txt = f.replace('.npy','.txt')
        print(" - {}".format(filename_txt))
        cal = apply_calibration(f,False)
        cal.dump_text_file(filename_txt)

@cli.command(name="plotter")
@click.argument('ifile')
@click.option('-a','--allpixels', is_flag=True, help='Create the calibration curve for all the pixels (in a 2D histogram)')
@click.option('-d','--distribution-plots', is_flag=True, help='Create 1-dim distributions for the calibration constants')
def plotter_command(ifile, allpixels, distribution_plots):
    """Plot calibration constants (using the calibration file prepared by the )

    ifile: Input calibration file created with the command `convert`
    """ 
    c = calibration_plotter(ifile)
    if allpixels:
        c.plot_all_calibration_curves()
    elif distribution_plots:
        c.plot_distributions()


if __name__ == "__main__":
    cli()

